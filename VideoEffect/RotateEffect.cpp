#include "VideoEffect/RotateEffect.h"

#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QCheckBox>
#include <QtCore/QDateTime>

namespace {

constexpr int ROTATEEFFECT_INITIAL_SPEED = 80;
constexpr bool ROTATEEFFECT_INITIAL_ENABLED = true;

class RotateEffectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit RotateEffectWidget(QWidget *parent = nullptr)
        : QWidget(parent)
    {
        auto speedSpinBox = new QSpinBox;
        speedSpinBox->setRange(1, 100);
        speedSpinBox->setValue(ROTATEEFFECT_INITIAL_SPEED);

        auto enableCheckBox = new QCheckBox("Enable");
        enableCheckBox->setChecked(ROTATEEFFECT_INITIAL_ENABLED);

        auto mainLayout = new QGridLayout;

        mainLayout->addWidget(enableCheckBox, 0, 0);
        mainLayout->addWidget(new QLabel("Speed:"), 1, 0);
        mainLayout->addWidget(speedSpinBox, 1, 1);

        this->setLayout(mainLayout);

        QObject::connect(speedSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &RotateEffectWidget::speedChanged);
        QObject::connect(enableCheckBox, &QCheckBox::stateChanged, this, &RotateEffectWidget::handleEnabledStateChanged);
    }

    ~RotateEffectWidget() override = default;

Q_SIGNALS:
    void speedChanged(int newSpeed);
    void enableChanged(bool enable);

private Q_SLOT:
    void handleEnabledStateChanged(int newState) {
        Q_EMIT this->enableChanged(newState == Qt::Checked);
    }
};

}

MultimediaDemo::VideoEffect::RotateEffect::RotateEffect(QObject *parent)
    : MultimediaDemo::VideoEffect::AbstractVideoEffect(parent),
      m_speed(1000),
      m_timePoint(-1),
      m_currentAngle(0),
      m_enabled(!ROTATEEFFECT_INITIAL_ENABLED)
{
    this->setSpeed(ROTATEEFFECT_INITIAL_SPEED);
    this->setEnabled(ROTATEEFFECT_INITIAL_ENABLED);
}

MultimediaDemo::VideoEffect::RotateEffect::~RotateEffect()
{
}

QTransform MultimediaDemo::VideoEffect::RotateEffect::createPainterTransform(const QSize &size)
{
    if(!this->m_enabled)
        return {};

    QTransform t;

    t.translate(size.width() / 2, size.height() / 2);
    t.rotate(this->calculateAngle());
    t.translate(-size.width() / 2, -size.height() / 2);

    return t;
}

void MultimediaDemo::VideoEffect::RotateEffect::modifyImage(QImage &image)
{
    Q_UNUSED(image)
}

QWidget *MultimediaDemo::VideoEffect::RotateEffect::createEditor(QWidget *parent)
{
    if(this->m_editor == nullptr) {
        auto newEditor = new RotateEffectWidget(parent);

        QObject::connect(newEditor, &RotateEffectWidget::speedChanged, this, &MultimediaDemo::VideoEffect::RotateEffect::setSpeed);
        QObject::connect(newEditor, &RotateEffectWidget::enableChanged, this, &MultimediaDemo::VideoEffect::RotateEffect::setEnabled);

        this->m_editor = newEditor;
    }

    return this->m_editor;
}

void MultimediaDemo::VideoEffect::RotateEffect::setSpeed(int newSpeed)
{
    this->m_speed = 101 - std::min(100, std::max(1, newSpeed));
}

void MultimediaDemo::VideoEffect::RotateEffect::setEnabled(bool enabled)
{
    if(this->m_enabled == enabled)
        return;

    if(enabled)
        this->m_timePoint = -1;

    this->m_enabled = enabled;
}

qreal MultimediaDemo::VideoEffect::RotateEffect::calculateAngle()
{
    const qint64 currentTimepoint = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();

    if(this->m_timePoint == -1)
        this->m_timePoint = currentTimepoint;

    const qint64 degreesToRotate = (currentTimepoint - this->m_timePoint) / this->m_speed;

    if(degreesToRotate > 0) {
        this->m_currentAngle += degreesToRotate;
        this->m_currentAngle %= 360;

        this->m_timePoint = currentTimepoint;
    }

    return static_cast<qreal>(this->m_currentAngle);
}

#include "RotateEffect.moc"
