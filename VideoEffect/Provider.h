#ifndef MULTIMEDIADEMO_VIDEOEFFECT_PROVIDER_H_INCLUDED
#define MULTIMEDIADEMO_VIDEOEFFECT_PROVIDER_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtCore/QVector>
#include <QtCore/QString>

namespace MultimediaDemo {
namespace VideoEffect {

struct EffectInfo {
    int id;
    QString name;
    QString description;
};

class Provider
{
public:
    static QVector<MultimediaDemo::VideoEffect::EffectInfo> getAllAvailableEffects();
    static MultimediaDemo::VideoEffect::AbstractVideoEffect *createEffect(int id, QObject *parent = nullptr);
};

} /* VideoEffect */
} /* MultimediaDemo */

Q_DECLARE_METATYPE(MultimediaDemo::VideoEffect::EffectInfo)

#endif /* MULTIMEDIADEMO_VIDEOEFFECT_PROVIDER_H_INCLUDED */
