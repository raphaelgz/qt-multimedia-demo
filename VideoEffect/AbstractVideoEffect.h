#ifndef MULTIMEDIADEMO_VIDEOEFFECT_ABSTRACTVIDEOEFFECT_H_INCLUDED
#define MULTIMEDIADEMO_VIDEOEFFECT_ABSTRACTVIDEOEFFECT_H_INCLUDED

#include <QtWidgets/QWidget>
#include <QtGui/QTransform>
#include <QtGui/QImage>
#include <QtCore/QObject>
#include <QtCore/QSize>

namespace MultimediaDemo {
namespace VideoEffect {

class AbstractVideoEffect : public QObject
{
    Q_OBJECT

public:
    explicit AbstractVideoEffect(QObject *parent = nullptr);
    virtual ~AbstractVideoEffect();

    virtual QTransform createPainterTransform(const QSize &size) = 0;
    virtual void modifyImage(QImage &image) = 0;
    virtual QWidget *createEditor(QWidget *parent) = 0;
};

} /* VideoEffect */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_VIDEOEFFECT_ABSTRACTVIDEOEFFECT_H_INCLUDED */
