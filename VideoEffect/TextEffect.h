#ifndef MULTIMEDIADEMO_VIDEOEFFECT_TEXTEFFECT_H_INCLUDED
#define MULTIMEDIADEMO_VIDEOEFFECT_TEXTEFFECT_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtCore/QPointer>

namespace MultimediaDemo {
namespace VideoEffect {

class TextEffect final : public MultimediaDemo::VideoEffect::AbstractVideoEffect
{
    Q_OBJECT

public:
    explicit TextEffect(QObject *parent = nullptr);
    ~TextEffect() override;

    QTransform createPainterTransform(const QSize &size) override;
    void modifyImage(QImage &image) override;
    QWidget *createEditor(QWidget *parent) override;

public Q_SLOTS:
    void setText(const QString &text);
    void setFont(const QFont &f);

private:
    QFont m_font;
    QString m_text;
    QPointer<QWidget> m_editor;
};

} /* VideoEffect */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_VIDEOEFFECT_TEXTEFFECT_H_INCLUDED */
