#ifndef MULTIMEDIADEMO_VIDEOEFFECT_ROTATEEFFECT_H_INCLUDED
#define MULTIMEDIADEMO_VIDEOEFFECT_ROTATEEFFECT_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtCore/QPointer>

namespace MultimediaDemo {
namespace VideoEffect {

class RotateEffect final : public MultimediaDemo::VideoEffect::AbstractVideoEffect
{
    Q_OBJECT

public:
    explicit RotateEffect(QObject *parent = nullptr);
    ~RotateEffect() override;

    QTransform createPainterTransform(const QSize &size) override;
    void modifyImage(QImage &image) override;
    QWidget *createEditor(QWidget *parent) override;

public Q_SLOTS:
    void setSpeed(int newSpeed);
    void setEnabled(bool enabled);

private:
    qreal calculateAngle();

    int m_speed;
    QPointer<QWidget> m_editor;
    qint64 m_timePoint;
    int m_currentAngle;
    bool m_enabled;
};

} /* VideoEffect */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_VIDEOEFFECT_ROTATEEFFECT_H_INCLUDED */
