#include "VideoEffect/Provider.h"
#include "VideoEffect/AbstractVideoEffect.h"
#include "VideoEffect/RotateEffect.h"
#include "VideoEffect/TextEffect.h"

#include <QtCore/QMap>

namespace {

struct AllKnownEffects
{
    struct Data {
        QString name;
        QString description;
        MultimediaDemo::VideoEffect::AbstractVideoEffect *(*factory)(QObject *);
    };

    AllKnownEffects() {
        this->map.insert(1, { "Rotate",
                              "Rotate the video until stopped",
                              [](QObject *obj) -> MultimediaDemo::VideoEffect::AbstractVideoEffect * {
                                  return new MultimediaDemo::VideoEffect::RotateEffect(obj);
                              }});

        this->map.insert(2, { "Custom text",
                              "Write a custom text over the video",
                              [](QObject *obj) -> MultimediaDemo::VideoEffect::AbstractVideoEffect * {
                                  return new MultimediaDemo::VideoEffect::TextEffect(obj);
                              }});
    }

    QMap<int, AllKnownEffects::Data> map;
};

}

Q_GLOBAL_STATIC(AllKnownEffects, allKnownEffects)

QVector<MultimediaDemo::VideoEffect::EffectInfo> MultimediaDemo::VideoEffect::Provider::getAllAvailableEffects()
{
    AllKnownEffects *p = allKnownEffects();

    if(p == nullptr)
        return {};

    QVector<MultimediaDemo::VideoEffect::EffectInfo> infoList;

    for(auto it = p->map.constBegin(); it != p->map.constEnd(); ++it) {
        MultimediaDemo::VideoEffect::EffectInfo e;

        e.id = it.key();
        e.name = it.value().name;
        e.description = it.value().description;

        infoList.append(e);
    }

    return infoList;
}

MultimediaDemo::VideoEffect::AbstractVideoEffect *MultimediaDemo::VideoEffect::Provider::createEffect(int id, QObject *parent)
{
    AllKnownEffects *p = allKnownEffects();

    if(p == nullptr)
        return nullptr;

    auto it = p->map.find(id);

    if(it == p->map.constEnd())
        return nullptr;

    return it->factory(parent);
}
