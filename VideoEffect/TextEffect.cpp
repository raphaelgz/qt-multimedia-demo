#include "VideoEffect/TextEffect.h"

#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtGui/QPainter>

namespace {

constexpr const char *TEXTEFFECT_INITIALFONT_NAME = "Times New Roman";
constexpr int TEXTEFFECT_INITIALFONT_SIZE = 14;

class TextEffectWidget final : public QWidget
{
    Q_OBJECT

public:
    explicit TextEffectWidget(QWidget *parent = nullptr)
        : QWidget(parent),
          m_font(TEXTEFFECT_INITIALFONT_NAME, TEXTEFFECT_INITIALFONT_SIZE)
    {
        auto textLineEdit = new QLineEdit;

        auto fontComboBox = new QFontComboBox;
        fontComboBox->setCurrentFont(m_font);

        this->m_fontSizeSpinBox = new QSpinBox;
        this->m_fontSizeSpinBox->setValue(TEXTEFFECT_INITIALFONT_SIZE);
        this->m_fontSizeSpinBox->setSuffix("pt");

        auto mainLayout = new QGridLayout;

        mainLayout->addWidget(new QLabel("Text:"), 0, 0);
        mainLayout->addWidget(textLineEdit, 0, 1, 1, 3);
        mainLayout->addWidget(new QLabel("Font:"), 1, 0);
        mainLayout->addWidget(fontComboBox, 1, 1);
        mainLayout->addWidget(new QLabel("Size:"), 1, 2);
        mainLayout->addWidget(this->m_fontSizeSpinBox, 1, 3);

        this->setLayout(mainLayout);

        QObject::connect(fontComboBox, &QFontComboBox::currentFontChanged, this, &TextEffectWidget::handleFontChangedOnComboBox);
        QObject::connect(this->m_fontSizeSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &TextEffectWidget::handleFontSizeChanged);
        QObject::connect(textLineEdit, &QLineEdit::textChanged, this, &TextEffectWidget::textChanged);
    }

    ~TextEffectWidget() override = default;

private Q_SLOTS:
    void handleFontSizeChanged(int newSize) {
        this->m_font.setPointSize(newSize);

        Q_EMIT this->fontChanged(this->m_font);
    }

    void handleFontChangedOnComboBox(const QFont &f) {
        this->m_font = f;
        this->m_font.setPointSize(this->m_fontSizeSpinBox->value());

        Q_EMIT this->fontChanged(this->m_font);
    }

Q_SIGNALS:
    void fontChanged(const QFont &newFont);
    void textChanged(const QString &newText);

private:
    QFont m_font;
    QSpinBox *m_fontSizeSpinBox;
};

}

MultimediaDemo::VideoEffect::TextEffect::TextEffect(QObject *parent)
    : MultimediaDemo::VideoEffect::AbstractVideoEffect(parent),
      m_font(TEXTEFFECT_INITIALFONT_NAME, TEXTEFFECT_INITIALFONT_SIZE)
{
}

MultimediaDemo::VideoEffect::TextEffect::~TextEffect()
{
}

QTransform MultimediaDemo::VideoEffect::TextEffect::createPainterTransform(const QSize &size)
{
    Q_UNUSED(size)
    return {};
}

void MultimediaDemo::VideoEffect::TextEffect::modifyImage(QImage &image)
{
    if(this->m_text.isEmpty())
        return;

    QPainter p(&image);

    p.setFont(this->m_font);
    p.drawText(image.rect(), Qt::AlignCenter, this->m_text);
}

QWidget *MultimediaDemo::VideoEffect::TextEffect::createEditor(QWidget *parent)
{
    if(this->m_editor == nullptr) {
        auto newEditor = new TextEffectWidget(parent);

        QObject::connect(newEditor, &TextEffectWidget::fontChanged, this, &MultimediaDemo::VideoEffect::TextEffect::setFont);
        QObject::connect(newEditor, &TextEffectWidget::textChanged, this, &MultimediaDemo::VideoEffect::TextEffect::setText);

        this->m_editor = newEditor;
    }

    return this->m_editor;
}

void MultimediaDemo::VideoEffect::TextEffect::setText(const QString &text)
{
    this->m_text = text;
}

void MultimediaDemo::VideoEffect::TextEffect::setFont(const QFont &f)
{
    this->m_font = f;
}

#include "TextEffect.moc"
