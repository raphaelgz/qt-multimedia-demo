#ifndef MULTIMEDIADEMO_WIDGET_VIDEOSURFACE_H_INCLUDED
#define MULTIMEDIADEMO_WIDGET_VIDEOSURFACE_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtMultimedia/QAbstractVideoSurface>

namespace MultimediaDemo {
namespace Widget {

class VideoSurface final : public QAbstractVideoSurface
{
    Q_OBJECT

public:
    explicit VideoSurface(QObject *parent = nullptr);
    ~VideoSurface() override;

    QSize sizeHint() const;
    QRect calculateVideoRect(const QRect &r) const;
    void paintCurrentFrame(QPainter *painter, const QRect &rect, const QVector<MultimediaDemo::VideoEffect::AbstractVideoEffect *> &effects);

    bool start(const QVideoSurfaceFormat &format) override;
    void stop() override;
    bool present(const QVideoFrame &frame) override;
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType type) const override;

Q_SIGNALS:
    void newFrameAvailable();

private:
    QVideoFrame m_frame;
    QImage::Format m_imageFormat;
    QRect m_surfaceFormatViewport;
};

} /* Widget */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_WIDGET_VIDEOSURFACE_H_INCLUDED */
