#include "Widget/VideoEffectsConfigurationWidget.h"
#include "VideoEffect/Provider.h"

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QGroupBox>

#include <memory>

MultimediaDemo::Widget::VideoEffectsConfigurationWidget::VideoEffectsConfigurationWidget(QWidget *parent)
    : QWidget(parent)
{
    auto chooseEffectLabel = new QLabel("Effect:", this);
    this->m_effectsComboBox = new QComboBox(this);
    auto chooseEffectAddButton = new QPushButton("Add", this);

    auto chooseEffectLayout = new QHBoxLayout;
    chooseEffectLayout->addWidget(chooseEffectLabel);
    chooseEffectLayout->addWidget(this->m_effectsComboBox);
    chooseEffectLayout->addWidget(chooseEffectAddButton);

    /*
    * TODO: Find a way to remove this hack. We need a way to make the QSpacerItem stay always at the bottom of the layout.
    */
    this->m_dummyLayoutSpacer = new QWidget;

    this->m_mainLayout = new QVBoxLayout;
    this->m_mainLayout->addLayout(chooseEffectLayout);
    this->m_mainLayout->addWidget(this->m_dummyLayoutSpacer, 1);

    this->setLayout(this->m_mainLayout);

    for(auto i : MultimediaDemo::VideoEffect::Provider::getAllAvailableEffects())
        this->m_effectsComboBox->addItem(i.name, QVariant::fromValue(i));

    QObject::connect(chooseEffectAddButton, &QPushButton::clicked, this, &MultimediaDemo::Widget::VideoEffectsConfigurationWidget::handleEffectAddRequested);
}

MultimediaDemo::Widget::VideoEffectsConfigurationWidget::~VideoEffectsConfigurationWidget()
{
}

void MultimediaDemo::Widget::VideoEffectsConfigurationWidget::handleEffectAddRequested()
{
    QVariant currentData = this->m_effectsComboBox->currentData();

    if(!currentData.isValid())
        return;

    auto effectInfo = currentData.value<MultimediaDemo::VideoEffect::EffectInfo>();
    std::unique_ptr<MultimediaDemo::VideoEffect::AbstractVideoEffect> effect(MultimediaDemo::VideoEffect::Provider::createEffect(effectInfo.id, this));

    if(effect == nullptr) {
        QMessageBox::critical(this, "Error", "Failed to create the effect");
        return;
    }

    this->m_effectsComboBox->removeItem(this->m_effectsComboBox->currentIndex());

    QWidget *effectEditor = effect->createEditor(nullptr);

    if(effectEditor == nullptr) {
        QMessageBox::critical(this, "Error", "Failed to create the effect editor");
        return;
    }

    auto effectGroupBoxLayout = new QVBoxLayout;
    effectGroupBoxLayout->addWidget(effectEditor);

    auto effectGroupBox = new QGroupBox(effectInfo.name);
    effectGroupBox->setLayout(effectGroupBoxLayout);

    this->m_mainLayout->removeWidget(this->m_dummyLayoutSpacer);
    this->m_mainLayout->addWidget(effectGroupBox);
    this->m_mainLayout->addWidget(this->m_dummyLayoutSpacer, 1);

    Q_EMIT effectAdded(effect.release());
}
