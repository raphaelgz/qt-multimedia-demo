#include "Widget/VideoSurface.h"

#include <QtMultimedia/QVideoSurfaceFormat>
#include <QtGui/QPainter>

MultimediaDemo::Widget::VideoSurface::VideoSurface(QObject *parent)
    : QAbstractVideoSurface(parent),
      m_imageFormat(QImage::Format_Invalid)
{
}

MultimediaDemo::Widget::VideoSurface::~VideoSurface()
{
}

QSize MultimediaDemo::Widget::VideoSurface::sizeHint() const
{
    return this->surfaceFormat().sizeHint();
}

QRect MultimediaDemo::Widget::VideoSurface::calculateVideoRect(const QRect &r) const
{
    QSize videoSize = this->surfaceFormat().sizeHint();
    videoSize.scale(r.size().boundedTo(videoSize), Qt::KeepAspectRatio);

    QRect videoRect = QRect(QPoint(0, 0), videoSize);
    videoRect.moveCenter(r.center());

    return videoRect;
}

void MultimediaDemo::Widget::VideoSurface::paintCurrentFrame(QPainter *painter, const QRect &rect, const QVector<MultimediaDemo::VideoEffect::AbstractVideoEffect *> &effects)
{
    if(this->m_frame.map(QAbstractVideoBuffer::ReadOnly)) {
        QImage img(this->m_frame.bits(),
                   this->m_frame.width(),
                   this->m_frame.height(),
                   this->m_frame.bytesPerLine(),
                   this->m_imageFormat);

        for(auto &effect : effects)
            effect->modifyImage(img);

        painter->drawImage(rect, img, this->m_surfaceFormatViewport);

        this->m_frame.unmap();
    }
}

bool MultimediaDemo::Widget::VideoSurface::present(const QVideoFrame &frame)
{
    if(this->surfaceFormat().pixelFormat() != frame.pixelFormat() || this->surfaceFormat().frameSize() != frame.size()) {
        this->setError(QAbstractVideoSurface::IncorrectFormatError);
        this->stop();

        return false;
    }

    this->m_frame = frame;
    Q_EMIT this->newFrameAvailable();

    return true;
}

QList<QVideoFrame::PixelFormat> MultimediaDemo::Widget::VideoSurface::supportedPixelFormats(QAbstractVideoBuffer::HandleType type) const
{
    if(type == QAbstractVideoBuffer::NoHandle) {
        QList<QVideoFrame::PixelFormat> list;

        for(auto f : { QVideoFrame::Format_ARGB32,
                       QVideoFrame::Format_ARGB32_Premultiplied,
                       QVideoFrame::Format_RGB32,
                       QVideoFrame::Format_RGB24,
                       QVideoFrame::Format_RGB565,
                       QVideoFrame::Format_RGB555,
                       QVideoFrame::Format_ARGB8565_Premultiplied,
                       QVideoFrame::Format_BGRA32,
                       QVideoFrame::Format_BGRA32_Premultiplied,
                       QVideoFrame::Format_BGR32,
                       QVideoFrame::Format_BGR24,
                       QVideoFrame::Format_BGR565,
                       QVideoFrame::Format_BGR555,
                       QVideoFrame::Format_BGRA5658_Premultiplied,
                       QVideoFrame::Format_AYUV444,
                       QVideoFrame::Format_AYUV444_Premultiplied,
                       QVideoFrame::Format_YUV444,
                       QVideoFrame::Format_YUV420P,
                       QVideoFrame::Format_YV12,
                       QVideoFrame::Format_UYVY,
                       QVideoFrame::Format_YUYV,
                       QVideoFrame::Format_NV12,
                       QVideoFrame::Format_NV21,
                       QVideoFrame::Format_IMC1,
                       QVideoFrame::Format_IMC2,
                       QVideoFrame::Format_IMC3,
                       QVideoFrame::Format_IMC4,
                       QVideoFrame::Format_Y8,
                       QVideoFrame::Format_Y16,
                       QVideoFrame::Format_Jpeg,
                       QVideoFrame::Format_CameraRaw,
                       QVideoFrame::Format_AdobeDng })
        {
            if(QVideoFrame::imageFormatFromPixelFormat(f) != QImage::Format_Invalid)
                list.append(f);
        }

        return list;
    }

    return {};
}

bool MultimediaDemo::Widget::VideoSurface::start(const QVideoSurfaceFormat &format)
{
    auto imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    auto frameSize = format.frameSize();

    if(imageFormat == QImage::Format_Invalid || frameSize.isEmpty())
        return false;

    this->m_imageFormat = imageFormat;
    this->m_surfaceFormatViewport = format.viewport();

    return QAbstractVideoSurface::start(format);
}

void MultimediaDemo::Widget::VideoSurface::stop()
{
    this->m_frame = {};
}
