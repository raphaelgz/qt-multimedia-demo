#include "Widget/VideoWidget.h"
#include "Widget/VideoSurface.h"

#include <QtMultimedia/QVideoSurfaceFormat>
#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>

MultimediaDemo::Widget::VideoWidget::VideoWidget(QWidget *parent)
    : QWidget(parent)
{
    this->m_videoSurface = new MultimediaDemo::Widget::VideoSurface(this);
    this->m_videoRect = this->m_videoSurface->calculateVideoRect(this->rect());

    auto p = this->palette();

    p.setColor(QPalette::Background, Qt::black);

    this->setPalette(p);

    QObject::connect(this->m_videoSurface, &MultimediaDemo::Widget::VideoSurface::newFrameAvailable,
                     this, &MultimediaDemo::Widget::VideoWidget::handleNewFrame);

    QObject::connect(this->m_videoSurface, &MultimediaDemo::Widget::VideoSurface::activeChanged,
                     this, &MultimediaDemo::Widget::VideoWidget::handleSurfaceActiveStateChange);
}

MultimediaDemo::Widget::VideoWidget::~VideoWidget()
{
}

QAbstractVideoSurface *MultimediaDemo::Widget::VideoWidget::getVideoSurface()
{
    return this->m_videoSurface;
}

const QAbstractVideoSurface *MultimediaDemo::Widget::VideoWidget::getVideoSurface() const
{
    return this->m_videoSurface;
}

void MultimediaDemo::Widget::VideoWidget::addEffect(MultimediaDemo::VideoEffect::AbstractVideoEffect *effect)
{
    effect->setParent(this);
    this->m_effects.append(effect);
}

QSize MultimediaDemo::Widget::VideoWidget::sizeHint() const
{
    return this->m_videoSurface->sizeHint();
}

void MultimediaDemo::Widget::VideoWidget::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);

    if(!this->m_videoSurface->isActive()) {
        painter.fillRect(e->rect(), this->palette().background());
        return;
    }

    for(const auto &effect : this->m_effects) {
        auto t = effect->createPainterTransform(this->size());

        if(!t.isIdentity())
            painter.setTransform(t, true);
    }

    if(!this->m_videoRect.contains(e->rect())) {
        QRegion region = e->region().subtracted(this->m_videoRect);

        for(auto r : region.rects())
            painter.fillRect(r, this->palette().background());
    }

    this->m_videoSurface->paintCurrentFrame(&painter, this->m_videoRect, this->m_effects);
}

void MultimediaDemo::Widget::VideoWidget::resizeEvent(QResizeEvent *e)
{
    QWidget::resizeEvent(e);

    this->updateVideoRect();
}

void MultimediaDemo::Widget::VideoWidget::handleNewFrame()
{
    this->repaint();
}

void MultimediaDemo::Widget::VideoWidget::handleSurfaceActiveStateChange(bool active)
{
    if(active) {
        this->updateGeometry();
        this->updateVideoRect();
    } else {
        this->update();
    }
}

void MultimediaDemo::Widget::VideoWidget::updateVideoRect()
{
    this->m_videoRect = this->m_videoSurface->calculateVideoRect(this->rect());
}
