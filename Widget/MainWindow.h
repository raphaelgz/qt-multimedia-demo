#ifndef MULTIMEDIADEMO_WIDGET_MAINWINDOW_H_INCLUDED
#define MULTIMEDIADEMO_WIDGET_MAINWINDOW_H_INCLUDED

#include <QtWidgets/QMainWindow>

namespace MultimediaDemo {
namespace Widget {

class VideoPlayerWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private Q_SLOTS:
    void openFile();

private:
    MultimediaDemo::Widget::VideoPlayerWidget *m_videoPlayer;
};

} /* Widget */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_WIDGET_MAINWINDOW_H_INCLUDED */
