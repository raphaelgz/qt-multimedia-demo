#ifndef MULTIMEDIADEMO_WIDGET_VIDEOWIDGET_H_INCLUDED
#define MULTIMEDIADEMO_WIDGET_VIDEOWIDGET_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtMultimedia/QAbstractVideoSurface>
#include <QtWidgets/QWidget>

namespace MultimediaDemo {
namespace Widget {

class VideoSurface;

class VideoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit VideoWidget(QWidget *parent = nullptr);
    ~VideoWidget() override;

    QAbstractVideoSurface *getVideoSurface();
    const QAbstractVideoSurface *getVideoSurface() const;
    void addEffect(MultimediaDemo::VideoEffect::AbstractVideoEffect *effect);

    QSize sizeHint() const override;
    void paintEvent(QPaintEvent *e) override;
    void resizeEvent(QResizeEvent *e) override;

private Q_SLOTS:
    void handleNewFrame();
    void handleSurfaceActiveStateChange(bool active);

private:
    void updateVideoRect();

    QRect m_videoRect;
    MultimediaDemo::Widget::VideoSurface *m_videoSurface;
    QVector<MultimediaDemo::VideoEffect::AbstractVideoEffect *> m_effects;
};

} /* Widget */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_WIDGET_VIDEOWIDGET_H_INCLUDED */
