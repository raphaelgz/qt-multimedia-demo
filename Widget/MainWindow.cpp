#include "Widget/MainWindow.h"
#include "Widget/VideoPlayerWidget.h"
#include "Widget/VideoEffectsConfigurationWidget.h"

#include <QtWidgets/QDockWidget>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QScrollArea>
#include <QtCore/QStandardPaths>
#include <QtCore/QDir>

MultimediaDemo::Widget::MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    auto fileMenu = this->menuBar()->addMenu("File");

    auto fileOpenAction = fileMenu->addAction("Open...");
    fileOpenAction->setShortcut(QKeySequence::Open);

    this->m_videoPlayer = new MultimediaDemo::Widget::VideoPlayerWidget(this);

    auto dockWidget = new QDockWidget("Effects", this);
    auto dockScrollArea = new QScrollArea;
    auto effectsConfigWidget = new MultimediaDemo::Widget::VideoEffectsConfigurationWidget;

    dockScrollArea->setWidget(effectsConfigWidget);
    dockWidget->setWidget(dockScrollArea);
    dockWidget->setWidget(effectsConfigWidget);

    this->addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, dockWidget);
    this->setCentralWidget(this->m_videoPlayer);

    QObject::connect(fileOpenAction, &QAction::triggered, this, &MultimediaDemo::Widget::MainWindow::openFile);
    QObject::connect(effectsConfigWidget, &MultimediaDemo::Widget::VideoEffectsConfigurationWidget::effectAdded,
                     this->m_videoPlayer, &MultimediaDemo::Widget::VideoPlayerWidget::addEffect);
}

MultimediaDemo::Widget::MainWindow::~MainWindow()
{
}

void MultimediaDemo::Widget::MainWindow::openFile()
{
    QFileDialog dlg(this, "Open Movie");

    dlg.setMimeTypeFilters(this->m_videoPlayer->supportedMimeTypes());
    dlg.setAcceptMode(QFileDialog::AcceptOpen);
    dlg.setFileMode(QFileDialog::ExistingFile);
    dlg.setDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).value(0, QDir::homePath()));

    if(dlg.exec() == QFileDialog::Accepted)
        this->m_videoPlayer->open(dlg.selectedUrls().constFirst());
}
