#include "Widget/VideoPlayerWidget.h"
#include "Widget/VideoWidget.h"

#include <QtMultimediaWidgets/QVideoWidget>
#include <QtMultimedia/QMediaPlayer>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStyle>
#include <QtWidgets/QMessageBox>
#include <QtCore/QTime>

#include <limits>

namespace {

constexpr auto MULTIMEDIADEMO_MEDIAPLAYER_FLAGS = QMediaPlayer::VideoSurface;

int multimediademo_milliseconds_to_slider_seconds(qint64 ms)
{
    return std::min(ms / 1000, static_cast<long long>(std::numeric_limits<int>::max()));
}

qint64 multimediademo_slider_seconds_to_milliseconds(int s)
{
    return s * 1000;
}

}

MultimediaDemo::Widget::VideoPlayerWidget::VideoPlayerWidget(QWidget *parent)
    : QWidget(parent)
{
    this->m_videoWidget = new MultimediaDemo::Widget::VideoWidget();

    this->m_mediaPlayer = new QMediaPlayer(this, MULTIMEDIADEMO_MEDIAPLAYER_FLAGS);
    this->m_mediaPlayer->setVideoOutput(this->m_videoWidget->getVideoSurface());

    this->m_playButton = new QPushButton;
    this->m_playButton->setIcon(this->style()->standardIcon(QStyle::SP_MediaPlay));
    this->m_playButton->setEnabled(false);

    this->m_pauseButton = new QPushButton;
    this->m_pauseButton->setIcon(this->style()->standardIcon(QStyle::SP_MediaPause));
    this->m_pauseButton->setEnabled(false);

    this->m_stopButton = new QPushButton;
    this->m_stopButton->setIcon(this->style()->standardIcon(QStyle::SP_MediaStop));
    this->m_stopButton->setEnabled(false);

    this->m_timeSlider = new QSlider(Qt::Horizontal);
    this->m_timeSlider->setSingleStep(1);
    this->m_timeSlider->setPageStep(5);
    this->m_timeSlider->setTracking(true);
    this->m_timeSlider->setEnabled(false);

    this->m_timeLabel = new QLabel;
    this->m_timeLabel->setText("--:--:--");

    auto buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(this->m_playButton);
    buttonsLayout->addWidget(this->m_pauseButton);
    buttonsLayout->addWidget(this->m_stopButton);
    buttonsLayout->addWidget(this->m_timeSlider);
    buttonsLayout->addWidget(this->m_timeLabel);

    auto layout = new QVBoxLayout;

    layout->addWidget(this->m_videoWidget, 1);
    layout->addLayout(buttonsLayout);

    this->setLayout(layout);

    /*
    QObject::connect(this->m_mediaPlayer, QOverload<QMediaPlayer::Error>::of(&QMediaPlayer::error),
                     this, &MultimediaDemo::VideoPlayerWidget::handleMediaPlayerError);*/
    QObject::connect(this->m_mediaPlayer, &QMediaPlayer::durationChanged,
                     this, &MultimediaDemo::Widget::VideoPlayerWidget::handleMediaDurationChanged);
    QObject::connect(this->m_mediaPlayer, &QMediaPlayer::positionChanged,
                     this, &MultimediaDemo::Widget::VideoPlayerWidget::handleMediaPositionChanged);
    QObject::connect(this->m_mediaPlayer, &QMediaPlayer::videoAvailableChanged,
                     this, &MultimediaDemo::Widget::VideoPlayerWidget::handleMediaVideoAvailableChanged);
    QObject::connect(this->m_mediaPlayer, &QMediaPlayer::seekableChanged,
                     this, &MultimediaDemo::Widget::VideoPlayerWidget::handleMediaSeekableChanged);

    QObject::connect(this->m_timeSlider, &QSlider::sliderMoved,
                     this, &MultimediaDemo::Widget::VideoPlayerWidget::handleSliderMoved);
    QObject::connect(this->m_timeSlider, &QSlider::actionTriggered,
                     this, &MultimediaDemo::Widget::VideoPlayerWidget::handleSliderActionTriggered);

    QObject::connect(this->m_playButton, &QPushButton::released, this, &MultimediaDemo::Widget::VideoPlayerWidget::play);
    QObject::connect(this->m_pauseButton, &QPushButton::released, this, &MultimediaDemo::Widget::VideoPlayerWidget::pause);
    QObject::connect(this->m_stopButton, &QPushButton::released, this, &MultimediaDemo::Widget::VideoPlayerWidget::stop);
}

MultimediaDemo::Widget::VideoPlayerWidget::~VideoPlayerWidget()
{
}

QStringList MultimediaDemo::Widget::VideoPlayerWidget::supportedMimeTypes() const
{
    return QMediaPlayer::supportedMimeTypes(MULTIMEDIADEMO_MEDIAPLAYER_FLAGS);
}

void MultimediaDemo::Widget::VideoPlayerWidget::open(const QUrl &url)
{
    this->m_mediaPlayer->setMedia(url);
}

void MultimediaDemo::Widget::VideoPlayerWidget::play()
{
    if(this->m_playButton->isEnabled())
        this->m_mediaPlayer->play();
}

void MultimediaDemo::Widget::VideoPlayerWidget::pause()
{
    if(this->m_pauseButton->isEnabled())
        this->m_mediaPlayer->pause();
}

void MultimediaDemo::Widget::VideoPlayerWidget::stop()
{
    if(this->m_stopButton->isEnabled())
        this->m_mediaPlayer->stop();
}

void MultimediaDemo::Widget::VideoPlayerWidget::addEffect(MultimediaDemo::VideoEffect::AbstractVideoEffect *effect)
{
    this->m_videoWidget->addEffect(effect);
}

/*void MultimediaDemo::VideoPlayerWidget::handleMediaPlayerError(QMediaPlayer::Error e)
{
    //QMessageBox::critical(this, "Error", QString("Oh no! %1 - %2").arg(e).arg(this->m_mediaPlayer->errorString()));
    qDebug() << e;
    qDebug() << this->m_mediaPlayer->errorString();
}*/

void MultimediaDemo::Widget::VideoPlayerWidget::handleMediaDurationChanged(qint64 ms)
{
    this->m_timeSlider->setRange(0, multimediademo_milliseconds_to_slider_seconds(ms));
}

void MultimediaDemo::Widget::VideoPlayerWidget::handleMediaPositionChanged(qint64 ms)
{
    this->m_timeLabel->setText(QTime::fromMSecsSinceStartOfDay(ms).toString());

    if(!this->m_timeSlider->isSliderDown())
        this->m_timeSlider->setValue(multimediademo_milliseconds_to_slider_seconds(ms));
}

void MultimediaDemo::Widget::VideoPlayerWidget::handleMediaVideoAvailableChanged(bool available)
{
    this->m_playButton->setEnabled(available);
    this->m_pauseButton->setEnabled(available);
    this->m_stopButton->setEnabled(available);
}

void MultimediaDemo::Widget::VideoPlayerWidget::handleMediaSeekableChanged(bool seekable)
{
    this->m_timeSlider->setEnabled(seekable);
}

void MultimediaDemo::Widget::VideoPlayerWidget::handleSliderMoved(int position)
{
    this->m_mediaPlayer->setPosition(multimediademo_slider_seconds_to_milliseconds(position));
}

void MultimediaDemo::Widget::VideoPlayerWidget::handleSliderActionTriggered(int action)
{
    Q_UNUSED(action);

    if(!this->m_timeSlider->isSliderDown())
        this->m_mediaPlayer->setPosition(multimediademo_slider_seconds_to_milliseconds(this->m_timeSlider->sliderPosition()));
}
