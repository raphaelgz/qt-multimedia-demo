#ifndef MULTIMEDIADEMO_WIDGET_VIDEOEFFECTSCONFIGURATIONWIDGET_H_INCLUDED
#define MULTIMEDIADEMO_WIDGET_VIDEOEFFECTSCONFIGURATIONWIDGET_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtWidgets/QWidget>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QVBoxLayout>

namespace MultimediaDemo {
namespace Widget {

class VideoEffectsConfigurationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit VideoEffectsConfigurationWidget(QWidget *parent = nullptr);
    ~VideoEffectsConfigurationWidget() override;

private Q_SLOTS:
    void handleEffectAddRequested();

Q_SIGNALS:
    void effectAdded(MultimediaDemo::VideoEffect::AbstractVideoEffect *effect);

private:
    QComboBox *m_effectsComboBox;
    QVBoxLayout *m_mainLayout;
    QWidget *m_dummyLayoutSpacer;
};

} /* Widget */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_WIDGET_VIDEOEFFECTSCONFIGURATIONWIDGET_H_INCLUDED */
