#ifndef MULTIMEDIADEMO_WIDGET_VIDEOPLAYERWIDGET_H_INCLUDED
#define MULTIMEDIADEMO_WIDGET_VIDEOPLAYERWIDGET_H_INCLUDED

#include "VideoEffect/AbstractVideoEffect.h"

#include <QtMultimedia/QMediaPlayer>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QLabel>
#include <QtCore/QUrl>

namespace MultimediaDemo {
namespace Widget {

class VideoWidget;

class VideoPlayerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit VideoPlayerWidget(QWidget *parent = nullptr);
    ~VideoPlayerWidget() override;

    QStringList supportedMimeTypes() const;

public Q_SLOTS:
    void open(const QUrl &url);
    void play();
    void pause();
    void stop();
    void addEffect(MultimediaDemo::VideoEffect::AbstractVideoEffect *effect);

private Q_SLOTS:
    //void handleMediaPlayerError(QMediaPlayer::Error e);
    void handleMediaDurationChanged(qint64 ms);
    void handleMediaPositionChanged(qint64 ms);
    void handleMediaVideoAvailableChanged(bool available);
    void handleMediaSeekableChanged(bool seekable);
    void handleSliderMoved(int position);
    void handleSliderActionTriggered(int action);

private:
    QPushButton *m_playButton;
    QPushButton *m_pauseButton;
    QPushButton *m_stopButton;
    QSlider *m_timeSlider;
    QLabel *m_timeLabel;
    QMediaPlayer *m_mediaPlayer;
    MultimediaDemo::Widget::VideoWidget *m_videoWidget;
};

} /* Widget */
} /* MultimediaDemo */

#endif /* MULTIMEDIADEMO_WIDGET_VIDEOPLAYERWIDGET_H_INCLUDED */
