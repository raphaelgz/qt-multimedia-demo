# Qt Multimedia Demo

This project is just a demonstration of some of the QtMultimedia capabilities.

Tested on:
  - Qt 5.12 + Windows 10 (using `.avi` videos)
  - Qt 5.11 + Ubuntu Focal (using `.mp4` videos)
