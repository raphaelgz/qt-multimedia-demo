#include "Widget/MainWindow.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MultimediaDemo::Widget::MainWindow m;

    QRect availableGeometry = QApplication::desktop()->availableGeometry(&m);

    m.resize(availableGeometry.width() / 2, availableGeometry.height() / 2);
    m.show();

    return app.exec();
}
